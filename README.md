# Baskervvol

This repository gathers sources and work files for the font Baskervvol.
Initiated during the *Genderfluid* workshop in November 2018 http://genderfluid.space, Baskervvol is a junction of the Baskervville designed by the ANRT https://github.com/anrt-type/ANRT-Baskervville including additions of non-binary glyphs.


### Bye Bye Binary team

Julie Colas, Louis Garrido, Ludi Loiseau, Édouard Naze, Marouchka Payen, Mathilde Quentin participated in Baskervvol during the Bye Bye Binary *Genderfluid* workshop in 2018. New glyphs and OpenType features are being added by Enzo Le Garrec and Camille Circlude for the purposes of a book in 2020.

In 2021, a specimen has been published by Surfaces Utiles on the occasion of the Perruque#24, publication about type design http://la-perruque.org/


### License

The fonts and related code are licenses under [Open Font License](http://scripts.sil.org/OFL).


### Credits

Please use this full credits if you use the Baskervvol. 

> (John Baskerville, Birmingham, c. 1750.) 
> Claude Jacob, Strasbourg, 1784. 
> ANRT (Alexis Faudot, Rémi Forte, Morgane Pierson, Rafael Ribas, Tanguy Vanlaeys, Rosalie Wagner), Nancy, 2017-2018. 
>️ Baskervvol BBB (Julie Colas, Camille Circlude, Louis Garrido, Enzo Le Garrec, Ludi Loiseau, Édouard Nazé, Marouchka Payen, Mathilde Quentin), Bruxelles 2018-2022. Ajout de glyphes non-binaires.



<3 Love & rage
